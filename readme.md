# Enquiry App

## Milestones

### Backend/API Side

* Create a new laravel application for API purpose
* Setup all required environment for enquiries app
* Create migration, model and controller for enquiries
* Create API endpoints for saving, listing and deleting enquiries
* Create mock testing for those API in insomnia or postman

### Angular Side

* Create a new angular application for enquiries app
* Implement the given enquiries app template in your app with css, images, etc.
* Create routing for the app
* Route to enquries list page from home page enquries button
* Create FormBuilder for the enquiry form
* Validate all fields using angular validation and show the validation errors on the bottom of corresponding - fields
* Create an enquiry service and register all API services in there.
* Submit and save the form
* Show the success messages in your app using any external library like https://craftpip.github.io/jquery-confirm/
* List all enquiries in enquiries page in an appropriate position
* Delete the enquiry with confirmation by pressing the delete button
